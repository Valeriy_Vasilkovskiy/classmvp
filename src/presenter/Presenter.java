package presenter;

import model.Model;
import view.IView;

public class Presenter implements IPresenter{
    private Model model;
    private IView pView;

    public Presenter(IView view) {
        this.pView = view;
        this.model = new Model();
    }

    @Override
    public String getString() {
       pView.Show(model.getString());
        return model.getString();
    }
}
