package presenter;

public interface IPresenter {
    String getString();
}
