package view;

import presenter.IPresenter;
import presenter.Presenter;

import javax.swing.*;
import java.awt.event.ActionEvent;


public class View extends JFrame implements IView {
    static JFrame jFrame;
    static JPanel jPanel;

    public static void main(String[] args) {
        IView view = new View();
        IPresenter presenter = new Presenter(view);

        jFrame = new JFrame("Class");
        jPanel = new JPanel();
        jPanel.setLayout(null);

        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        jFrame.setSize(300, 400);
        jFrame.setLocationRelativeTo(null);
        jFrame.setResizable(false);
        jFrame.add(jPanel);

        JTextField jTextField = new JTextField();
        jTextField.setBounds(30, 20, 250, 50);
        jTextField.setHorizontalAlignment(4);
        jPanel.add(jTextField);

        JButton jButtonShow = new JButton("Show Text");
        jButtonShow.setBounds(85, 90, 150, 60);
        jPanel.add(jButtonShow);

        jFrame.revalidate();
        jFrame.setVisible(true);

        jButtonShow.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                jTextField.setText(presenter.getString());
            }
        });
    }

    @Override
    public void Show(String string) {
        System.out.println(string);
    }
}
